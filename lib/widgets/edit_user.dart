import 'dart:async';
import 'package:flutter/material.dart';

import '/models/user.dart';
import '/utils/api.dart';


class EditUserScreen extends StatefulWidget {
    final User? _user;

    EditUserScreen(this._user);

    @override
    _EditUserScreenState createState() => _EditUserScreenState();
}

class _EditUserScreenState extends State<EditUserScreen> {
    Future? _futureUser;

    final _formKey = GlobalKey<FormState>();
    final _textNameController = TextEditingController();
    final _textEmailController = TextEditingController();
    final _textPhoneController = TextEditingController();
    final _textWebsiteController = TextEditingController();

    void updateUserDialog(BuildContext context) {
        setState(() {
            _futureUser = API().updateUser(
                id: widget._user!.id,
                name: _textNameController.text, 
                email: _textEmailController.text, 
                phone: _textPhoneController.text, 
                website: _textWebsiteController.text
            ).catchError((error){
                showSnackBar(context, error.message);
            });
        });
    }
    void showSnackBar(BuildContext context, String message){
        SnackBar snackBar = new SnackBar(
            content: Text(message),
            duration: Duration(milliseconds: 2000));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);   
    }
    
    @override
    Widget build(BuildContext context) {
        final FocusScopeNode focusNode = FocusScope.of(context);

        Widget nameField = TextFormField(
            decoration: InputDecoration(labelText: 'Name',),
            keyboardType: TextInputType.text,
            controller: _textNameController,
            onEditingComplete: focusNode.nextFocus,
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'This field is required.';
            }
        );

        Widget emailField = TextFormField(
            decoration: InputDecoration(labelText: 'Email',),
            keyboardType: TextInputType.emailAddress,
            controller: _textEmailController,
            onEditingComplete: focusNode.nextFocus,
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'This field is required.';
            }
        );

        Widget phoneField = TextFormField(
            decoration: InputDecoration(labelText: 'Phone',),
            keyboardType: TextInputType.text,
            controller: _textPhoneController,
            onEditingComplete: focusNode.nextFocus,
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'This field is required.';
            }
        );

        Widget websiteField = TextFormField(
            decoration: InputDecoration(labelText: 'Website',),
            keyboardType: TextInputType.text,
            controller: _textWebsiteController,
            onEditingComplete: focusNode.nextFocus,
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'This field is required.';
            }
        );

        Widget btnSubmit = Container(
            width: double.infinity,
            margin: EdgeInsets.all(5.0),
            child: SizedBox(
                child: ElevatedButton(
                    child: Text('Update'),
                    onPressed: () {
                        if (_formKey.currentState!.validate()) {
                            updateUserDialog(context);
                        } else {
                            showSnackBar(context, 'Error');
                        }
                    }
                ),
            ),
        );

        Widget updateUserForm = SingleChildScrollView(
            child: Form(
                key: _formKey,
                child: Column(
                    children: [
                        nameField,
                        emailField,
                        phoneField,
                        websiteField,
                        btnSubmit
                    ],
                )
            ),
        );

        Widget userUpdateView = FutureBuilder(
            future: _futureUser,
            builder: (context, snapshot) {
                if (_futureUser == null) {
                    return updateUserForm;
                } 
                
                if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasError) {
						showSnackBar(context, 'Could not update the user, restart the app.');
					} else if (snapshot.hasData){
                        print(snapshot.data);

                        Timer(Duration(seconds: 1), () {
                            showSnackBar(context, 'User updated successfully');
                            Navigator.pushNamed(context, '/');
                        });

                    } else {
                        return updateUserForm;
                    }
                }

                return Center(
					child: CircularProgressIndicator()
				);
            }
        );

        return Scaffold(
            appBar: AppBar(title: Text('Add User')),
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.all(16),
                child: userUpdateView
            ),
        );

    }
}



