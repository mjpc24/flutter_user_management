import 'dart:async';

import 'package:flutter/material.dart';

import '/utils/api.dart';



class AddUser extends StatefulWidget {
    @override
    AddUserState createState() => AddUserState();
}

class AddUserState extends State<AddUser> {
    Future? _futureUser;

    final _formKey = GlobalKey<FormState>();

    final _tffNameController = TextEditingController();
    final _tffEmailController = TextEditingController();
    final _tffPhone = TextEditingController();
    final _tffWebsite = TextEditingController();

    void addUser(BuildContext context){

        setState(() {
                _futureUser = API().addUser(
                name: _tffNameController.text,
                email: _tffEmailController.text,
                phone: _tffPhone.text,
                website: _tffWebsite.text
                ).catchError((error){
                    showSnackBar(context, error.message);
            });
        
        });

    }
    void showSnackBar(BuildContext context, String message){
        SnackBar snackBar = new SnackBar(
            content: Text(message),
            duration: Duration(milliseconds: 2000));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);   
    }
    @override
    Widget build(BuildContext context) {

        Widget tffName = TextFormField(
                decoration: InputDecoration(
                    labelText: 'Name',
                    labelStyle: TextStyle(
                    color: Colors.black
                    )    
                ),
                keyboardType: TextInputType.text,
                controller: _tffNameController , 
        );
        Widget tffEmail = TextFormField(
                decoration: InputDecoration(labelText: 'Email'),
                keyboardType: TextInputType.emailAddress,
                controller: _tffEmailController,
        );
        Widget tffPhone = TextFormField(
                decoration: InputDecoration(
                    labelText: 'Phone',
                    labelStyle: TextStyle(
                    color: Colors.black
                    )
                ),
                keyboardType: TextInputType.text,
                controller: _tffPhone,
            
        );
        Widget tffWebsite = TextFormField(
                decoration: InputDecoration(
                    labelText: 'Website',
                    labelStyle: TextStyle(
                    color: Colors.black
                    )
                    
                ),
                keyboardType: TextInputType.text,
                controller: _tffWebsite
        );
        Widget btnRegister = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 8.0),
            child: ElevatedButton(
                child: Text('Register'),
                onPressed: () {
                    addUser(context);
                }
            )
        );
        Widget formRegister = SingleChildScrollView(
            child: Form(
                key: _formKey,
                child: Column(
                    children: [
                        tffName,
                        tffEmail,
                        tffPhone,
                        tffWebsite,
                        btnRegister,
                        
                    ]
                )
            )
        );
        Widget registerView = FutureBuilder(
            future: _futureUser,
            builder:(context,snapshot){
                if(_futureUser == null){
                    return formRegister;
                }else if (snapshot.hasData == false){
                    print(snapshot.connectionState);
                    print(snapshot.hasData);
                    print(snapshot.hasError);
                    return formRegister;
                } else if(snapshot.hasError == true){
                    showSnackBar(context, 'Encountered an error');
                    return formRegister;
                }else {
                    Timer(Duration(seconds: 3),(){
                        print(snapshot.data);
                        // Text('registration successful, You will b redirected shortly back to the log in page.');
                        showSnackBar(context, 'Success');
                        Navigator.pushReplacementNamed(context, '/');        
                    });
                } 
                return Center(
                        child:CircularProgressIndicator()
                    );
                }
        );
        return Scaffold(
            appBar: AppBar(title: Text('New User Registration')),
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.all(16.0),
                child: registerView
            )
        );
    }

}