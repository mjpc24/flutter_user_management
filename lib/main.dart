import 'package:flutter/material.dart';
import 'package:flutter_user_management/widgets/add_user.dart';
// import 'package:flutter_user_management/widgets/edit_user.dart';

import '/screens/user_list_screen.dart';

void main() {
    runApp(App());
}

class App extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            initialRoute: '/',
            routes: {
                '/': (context) => UserListScreen(),
                '/add': (context) => AddUser(),
                // '/edit': (context) => EditUser()
            }
        );
    }
}
