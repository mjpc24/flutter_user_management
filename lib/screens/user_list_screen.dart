import 'package:flutter/material.dart';
import 'package:flutter_user_management/widgets/edit_user.dart';
import '/utils/api.dart';
import '/models/user.dart';

class UserListScreen extends StatefulWidget {


	@override
	UserListScreenState createState() => UserListScreenState();
}

class UserListScreenState extends State<UserListScreen> {
    Future? futureUsers;
    Future? individualUser;
    final refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    List<Widget> generateListTiles(List<User> users){
        List<Widget> listTiles = [];

        for (User user in users) {
			listTiles.add(ListTile(
				title: Text(user.name),
				subtitle: Text(user.email),
                trailing: IconButton(
                    onPressed:(){
                        // var route = MaterialPageRoute(builder: (BuildContext context) =>);
                        
                        individualUser =API().getUser(id: user.id);
                        Navigator.push(context, MaterialPageRoute(builder: (context) => EditUserScreen(user)));
                        
                        // Navigator.of(context).push(route);
                    },
                    icon: Icon(Icons.edit)
                ), 
                
			));
		}
            return listTiles;
    }    
    

	@override
	void initState() { 
		super.initState();

		WidgetsBinding.instance!.addPostFrameCallback((timestamp)  {
			setState(() {
                futureUsers = API().getUsers();
			});
		});
	}

	@override
	Widget build(BuildContext context) {
        Widget fbUserList = FutureBuilder(
            future: futureUsers,
            builder: (context,snapshot){
                if (snapshot.connectionState == ConnectionState.done){
                    if (snapshot.hasError){
                        return Center(
                            child: Text('Could not load the User list screen'),
                        );
                    }
                    return RefreshIndicator(
                        key: refreshIndicatorKey,
                        onRefresh: () async {
                            setState(() {
                                futureUsers = API().getUsers();
			                });
                        },
                        child:ListView(
                        padding: EdgeInsets.all(8),
                        children: generateListTiles(snapshot.data as List<User>)
                        ,
                        )
                    ); 
                }

                return Center(
                    child: CircularProgressIndicator()
                );


                
                // print(snapshot.connectionState);
                // print(snapshot.hasData);
                // print(snapshot.hasError);
                // print(snapshot.data);
                // return Text('You are in the user list screen');
            }
        );
		return Scaffold(
			appBar: AppBar(title: Text('User List')),
            drawer: Drawer(
                child: Column(
                    children: [
                        ListTile(
                                    title: Text('Add New User'),
                                    onTap: () async {
                                        Navigator.pushNamed(context, '/add');
                                    }
                        )
                    ],
                    
                ),
            ) ,
			body: Container(
                
                child: fbUserList)
		);
	}
}